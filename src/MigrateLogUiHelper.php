<?php

namespace Drupal\migrate_log_ui;

use Drupal\migrate\Plugin\MigrationInterface;

/**
 * MigrateLogUiHelper trait stores shared methods.
 */
trait MigrateLogUiHelper {

  /**
   * Get title of migration message DB table level.
   *
   * @param int $level
   *   Migration message level.
   *
   * @return string
   *   Corresponding message level title or level number for unmapped levels.
   */
  public function getMessageLevelTitle(int $level): string {
    $titles = $this->getMigrateMessageLevelTitleMap();
    if (isset($titles[$level])) {
      return $titles[$level];
    }
    return (string) $level;
  }

  /**
   * Getter for the map between migration message level and title.
   *
   * @return string[]
   *   Message level to title map.
   */
  public function getMigrateMessageLevelTitleMap(): array {
    $titles = [
      MigrationInterface::MESSAGE_NOTICE => 'notice',
      MigrationInterface::MESSAGE_WARNING => 'warning',
      MigrationInterface::MESSAGE_ERROR => 'error',
      MigrationInterface::MESSAGE_INFORMATIONAL => 'info',
    ];
    return $titles;
  }

}
