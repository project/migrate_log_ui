<?php

declare(strict_types = 1);

namespace Drupal\migrate_log_ui\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Url;
use Drupal\migrate\Exception\RequirementsException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Plugin\RequirementsInterface;
use Drupal\migrate_log_ui\MigrateLogUiHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Responses to Migrate Log UI routes.
 */
class MigrateLogUiController extends ControllerBase {

  use MigrateLogUiHelper;

  /**
   * Plugin manager for migration plugins.
   */
  protected MigrationPluginManagerInterface $migrationPluginManager;

  /**
   * The database service.
   */
  protected Connection $database;

  /**
   * Current HTTP request.
   */
  protected Request $request;

  /**
   * MigrateLogUiController constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migrationPluginManager
   *   Plugin manager for migration plugins.
   * @param \Drupal\Core\Database\Connection $database
   *   Drupal DB connection.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current HTTP request.
   */
  public function __construct(MigrationPluginManagerInterface $migrationPluginManager, Connection $database, Request $request) {
    $this->migrationPluginManager = $migrationPluginManager;
    $this->database = $database;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): MigrateLogUiController {
    return new static(
      $container->get('plugin.manager.migration'),
      $container->get('database'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * Smart listing of migration messages.
   *
   * @param string $migration
   *   Migration ID.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function logViewer(string $migration): array {
    $rows = [];
    $mapSourceFieldNames = [];
    $migration_plugin = $this->migrationPluginManager->createInstance($migration);
    $source_id_field_names = array_keys($migration_plugin->getSourcePlugin()->getIds());
    $column_number = 1;

    $filterQuery = $this->getFilterQuery();
    parse_str($filterQuery, $filters);

    $build['migrate_log_ui_filter_form'] = $this->formBuilder()->getForm('Drupal\migrate_log_ui\Form\MigrationMessageFilterForm', $filters);

    if (!empty($filters['group_by_message'])) {
      $header[] = [
        'data' => $this->t('Results'),
        'field' => 'results',
      ];
    }
    else {
      foreach ($source_id_field_names as $source_id_field_name) {
        $mapSourceFieldName = 'sourceid' . $column_number++;
        $header[] = [
          'data' => $source_id_field_name,
          'field' => $mapSourceFieldName,
        ];
        $mapSourceFieldNames[] = $mapSourceFieldName;
      }
    }
    $header[] = [
      'data' => $this->t('Level'),
      'field' => 'level',
    ];
    $header[] = [
      'data' => $this->t('Message'),
      'field' => 'message',
    ];

    $result = [];
    $rowCount = 0;
    $message_table = $migration_plugin->getIdMap()->messageTableName();
    if ($this->database->schema()->tableExists($message_table)) {
      $map_table = $migration_plugin->getIdMap()->mapTableName();
      /** @var \Drupal\Core\Database\Query\Select $query */
      $query = $this->database->select($message_table, 'msg');
      $query->innerJoin($map_table, 'map', 'msg.source_ids_hash=map.source_ids_hash');

      // Filter: Levels.
      if (!empty($filters['level'])) {
        $or_level_conditions = new Condition('OR');
        foreach ($filters['level'] as $level) {
          $or_level_conditions->condition('level', $level);
        }
        $query->condition($or_level_conditions);
      }
      // Filter: Source ID.
      if (!empty($filters['sourceid1'])) {
        $query->condition('map.sourceid1', trim($filters['sourceid1']));
      }

      // Filter: Newer than.
      $newerThanTimestamp = 0;
      if (!empty($filters['newer_than'])) {
        $dtString = trim($filters['newer_than']);
        if ($newerThanTimestamp = strtotime($dtString)) {
          $query->condition('map.last_imported', $newerThanTimestamp, '>');
        }
      }

      // Filter: Messages.
      if (!empty($filters['message1'])) {
        $condition_operator = 'LIKE';
        if (!empty($filters['negative_condition1'])) {
          $condition_operator = 'NOT LIKE';
        }
        $query->condition('message', $filters['message1'], $condition_operator);
      }
      if (!empty($filters['message2'])) {
        $condition_operator = 'LIKE';
        if (!empty($filters['negative_condition2'])) {
          $condition_operator = 'NOT LIKE';
        }
        $query->condition('message', $filters['message2'], $condition_operator);
      }
      // Filter: Grouping.
      if (!empty($filters['group_by_message'])) {
        $query->groupBy('message');
        // Define aggregated fields according to Drupal only_full_group_by mode.
        $query->addExpression('COUNT(*)', 'results');
        $query->addExpression('MIN(msg.level)', 'level');
        $query->fields('msg', ['message']);
      }
      else {
        // Define fields without group by.
        $query->fields('map', $mapSourceFieldNames);
        $query->fields('msg', ['message', 'level']);
      }

      // Get record count before paging.
      $rowCount = $query->countQuery()->execute()->fetchField();

      /** @var \Drupal\Core\Database\Query\PagerSelectExtender $query */
      $query = $query->extend('\Drupal\Core\Database\Query\PagerSelectExtender');
      $query = $query->limit(500);

      /** @var \Drupal\Core\Database\Query\TableSortExtender $query */
      $query = $query->extend('\Drupal\Core\Database\Query\TableSortExtender');
      $query = $query->orderByHeader($header);

      // Execution.
      $result = $query->execute();
      foreach ($result as $messageRow) {
        if (!empty($filters['group_by_message'])) {
          $row['results'] = [
            'data' => $messageRow->results,
          ];
        }
        else {
          foreach ($mapSourceFieldNames as $mapSourceFieldName) {
            $row[$mapSourceFieldName] = $messageRow->$mapSourceFieldName;
          }
        }
        $row['level'] = $this->getMessageLevelTitle((int) $messageRow->level);
        $row['message'] = $messageRow->message;
        $rows[] = $row;
      }

      // Show info about time filtering for easy verification, because
      // the filter value is not validated.
      if (!empty($filters['newer_than']) && $newerThanTimestamp > 0) {
        $build['newer_than_check'] = [
          '#type' => 'item',
          '#title' => $this->t('Displayed only newer than:'),
          '#plain_text' => date('Y-m-d H:i:s', $newerThanTimestamp) . " (timestamp: $newerThanTimestamp)",
        ];
      }

      $build['message_table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => ['id' => $message_table, 'class' => [$message_table]],
        '#empty' => $this->t('No messages for this migration.'),
        '#caption' => $this->t('Total of records: @count', ['@count' => $rowCount]),
      ];
      $build['message_pager'] = ['#type' => 'pager'];
      $build['#attached']['library'][] = 'migrate_log_ui/migrate_log_ui';
    }

    return $build;
  }

  /**
   * Action for migrate_log_ui.overview route.
   *
   * Prints table of all active migrations with useful data.
   *
   * @return array
   *   Page content.
   */
  public function overview(): array {
    try {
      $migrationList = $this->migrationsList();
    }
    catch (PluginNotFoundException $e) {
      throw new \Exception('No migrations found.');
    }

    $ignoredCountExists = NULL;
    $rows = [];
    $previousGroupName = '';
    foreach ($migrationList as $groupName => $migrations) {
      /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
      foreach ($migrations as $migration) {
        // Skip all disabled migrations.
        if ($migration->getStatus() == MigrationInterface::STATUS_DISABLED) {
          continue;
        }
        // Is ignoredCount() implemented?
        // The issue https://www.drupal.org/project/drupal/issues/3244624
        // adds new migrate map counters for imported (only) and ignored items.
        // We want to be ready to extend the table when the issue is fixed.
        if (is_null($ignoredCountExists)) {
          $idMap = $migration->getIdMap();
          $ignoredCountExists = method_exists($idMap, 'ignoredCount');
        }
        $migrationName = $migration->getBaseId();
        $link = Url::fromRoute('migrate_log_ui.messages', ['migration' => $migrationName]);
        $row = [];
        // Print the group name only for the 1st migration in the group.
        if ($previousGroupName !== $groupName) {
          $previousGroupName = $groupName;
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $groupName,
              '#suffix' => '</em>',
            ],
          ];
        }
        else {
          $row[] = [];
        }
        try {
          $totalCount = $migration->getSourcePlugin()->count();
        }
        catch (\Exception $e) {
          $totalCount = 'N/A';
        }
        $processedCount = $migration->getIdMap()->processedCount();
        $row[] = [
          'data' => [
            '#title' => $migrationName,
            '#type' => 'link',
            '#url' => $link,
          ],
        ];
        $row[] = [
          'data' => [
            '#markup' => $migration->getStatusLabel(),
          ],
        ];
        $row[] = [
          'data' => [
            '#markup' => $totalCount,
          ],
        ];
        $row[] = [
          'data' => [
            '#markup' => $processedCount,
          ],
        ];
        $row[] = [
          'data' => [
            '#markup' => $migration->getIdMap()->importedCount(),
          ],
        ];
        $row[] = [
          'data' => [
            '#markup' => $migration->getIdMap()->errorCount(),
          ],
        ];
        if ($ignoredCountExists) {
          $row[] = [
            'data' => [
              '#markup' => $migration->getIdMap()->ignoredCount(),
            ],
          ];
        }
        $row[] = [
          'data' => [
            '#markup' => (int) $totalCount - $processedCount,
          ],
        ];
        $row[] = [
          'data' => [
            '#markup' => $migration->getIdMap()->updateCount(),
          ],
        ];
        $row[] = [
          'data' => [
            '#markup' => $migration->getIdMap()->messageCount(),
          ],
        ];
        $rows[] = $row;
      }
    }
    $header = [
      'Group',
      'Migration',
      'Status',
      'Total',
      'Processed',
      'Imported',
      'Failed',
    ];
    if ($ignoredCountExists) {
      $header = array_merge($header, ['Ignored']);
    }
    $header = array_merge($header, [
      'Unprocessed',
      'To update',
      'Messages',
    ]);
    $build['migration-table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Get migration log viewer page title.
   *
   * @param string $migration
   *   Migration ID.
   *
   * @return string
   *   Page title.
   */
  public function getLogViewerTitle(string $migration): string {
    return "Messages for '$migration' migration";
  }

  /**
   * Builds a query for migration message filters based on URL query params.
   *
   * @return string
   *   URL query with form values.
   */
  public function getFilterQuery(): string {
    $query_params = $this->request->query->all();
    if (empty($query_params)) {
      return '';
    }
    unset($query_params['op']);
    unset($query_params['form_build_id']);
    unset($query_params['form_token']);
    unset($query_params['form_id']);
    return UrlHelper::buildQuery($query_params);
  }

  /**
   * Retrieve a list of active migrations.
   *
   * Simplified version of
   * \Drupal\migrate_tools\Drush\MigrateToolsCommands::migrationsList.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[][]
   *   An array keyed by migration group, each value containing an array of
   *   migrations or an empty array if no migrations match the input criteria.
   */
  protected function migrationsList(): array {
    $matched_migrations = [];
    // Get all migrations.
    $plugins = $this->migrationPluginManager->createInstances([]);
    $matched_migrations = $plugins;

    // Do not return any migrations which fail to meet requirements.
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    foreach ($matched_migrations as $id => $migration) {
      try {
        if ($migration->getSourcePlugin() instanceof RequirementsInterface) {
          $migration->getSourcePlugin()->checkRequirements();
        }
      }
      catch (RequirementsException | PluginNotFoundException $e) {
        unset($matched_migrations[$id]);
      }
    }

    // Sort the matched migrations by group.
    if (!empty($matched_migrations)) {
      foreach ($matched_migrations as $id => $migration) {
        $configured_group_id = empty($migration->migration_group) ? 'default' : $migration->migration_group;
        $migrations[$configured_group_id][$id] = $migration;
      }
    }
    return $migrations ?? [];
  }

}
