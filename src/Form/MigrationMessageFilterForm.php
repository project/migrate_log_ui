<?php

declare(strict_types = 1);

namespace Drupal\migrate_log_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\migrate_log_ui\MigrateLogUiHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the filter form for migration log viewer.
 */
class MigrationMessageFilterForm extends FormBase {

  use MigrateLogUiHelper;

  /**
   * Storage for current HTTP request.
   */
  protected Request $currentRequest;

  /**
   * MigrationMessageFilterForm constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current HTTP request.
   */
  public function __construct(Request $request) {
    $this->currentRequest = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): MigrationMessageFilterForm {
    return new static(
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_log_ui_message_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, array $filters = []) {

    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter log messages'),
      '#open' => TRUE,
    ];

    $migrateMessageLevelTitleMap = $this->getMigrateMessageLevelTitleMap();

    $form['filters']['section_1'] = [
      '#type' => 'fieldset',
      '#attributes' => ['class' => ['form--inline']],
    ];
    $form['filters']['section_1']['level'] = [
      '#type' => 'select',
      '#title' => 'Level',
      '#multiple' => TRUE,
      '#size' => count($migrateMessageLevelTitleMap),
      '#options' => $migrateMessageLevelTitleMap,
      '#default_value' => !empty($filters['level']) ? $filters['level'] : NULL,
    ];
    $form['filters']['section_1']['sourceid1'] = [
      '#title' => $this->t('Source key'),
      '#description' => $this->t('Value is trimmed'),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => !empty($filters['sourceid1']) ? $filters['sourceid1'] : '',
    ];
    $form['filters']['section_1']['message1'] = [
      '#title' => $this->t('Message contains'),
      '#description' => $this->t('E.g. Exact match, Start with%, %End with or %contains%'),
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => !empty($filters['message1']) ? $filters['message1'] : '',
    ];
    $form['filters']['section_1']['negative_condition1'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Negative'),
      '#default_value' => !empty($filters['negative_condition1']) ? $filters['negative_condition1'] : '',
    ];
    $form['filters']['section_1']['newer_than'] = [
      '#title' => $this->t('Newer than'),
      '#description' => $this->t('Show only records newer than this time. Use strtotime() format, like YYYY-MM-DD H:i:s, -1 week, last Monday, etc.'),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => !empty($filters['newer_than']) ? $filters['newer_than'] : '',
    ];
    $form['filters']['section_m2'] = [
      '#type' => 'details',
      '#title' => $this->t('One more message filter'),
      '#open' => FALSE,
      '#attributes' => ['class' => ['section_m2']],
    ];
    $form['filters']['section_m2']['wrapper'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['class' => ['form--inline']],
    ];
    $form['filters']['section_m2']['wrapper']['message2'] = [
      '#title' => $this->t('AND message contains'),
      '#description' => $this->t('E.g. Exact match, Start with%, %End with or %contains%'),
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => !empty($filters['message2']) ? $filters['message2'] : '',
    ];
    $form['filters']['section_m2']['wrapper']['negative_condition2'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Negative'),
      '#default_value' => !empty($filters['negative_condition2']) ? $filters['negative_condition2'] : '',
    ];

    $form['filters']['group_by_message'] = [
      '#type' => 'checkbox',
      '#title' => 'Group by message',
      '#default_value' => !empty($filters['group_by_message']) ? $filters['group_by_message'] : '',
    ];

    $form['filters']['actions'] = [
      '#type' => 'actions',
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];
    $form['filters']['actions']['reset'] = [
      '#type' => 'link',
      '#title' => 'Reset filters',
      '#url' => Url::fromRoute('<current>'),
      '#attributes' => [
        'class' => [
          'reset-link',
          'button',
        ],
      ],
    ];

    // We want to share form filters settings via URL with others.
    // @todo This disables formValidate method. Resolve later.
    $form['#method'] = 'get';

    // The after_build removes elements from GET parameters.
    $form['#after_build'] = ['::afterBuild'];

    return $form;
  }

  /**
   * Remove elements from being submitted as GET variables.
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    unset($element['form_token']);
    unset($element['form_build_id']);
    unset($element['form_id']);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   *
   * @todo This method is not used when form method is GET. Resolve later.
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    if (!empty($form_state->getValue('newer_than'))
      && !strtotime($form_state->getValue('newer_than'))) {
      $form_state->setError($form['filters']['section_1']['newer_than'], 'Invalid date time format');
    }
  }

}
