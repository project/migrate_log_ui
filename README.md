# Migrate log UI

This module allows easy viewing and filtering of migration messages. Migration messages can be filtered by source key, message type, message content. The filter can also be negative.

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/migrate_log_ui).

## Requirements (required)

This module requires the following modules enabled:

- drupal:migrate

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is required.


## Maintainers

- Martin Klima - [martin_klima](https://www.drupal.org/u/martin_klima)
- Wolfgang Ziegler - [fago](https://www.drupal.org/u/fago)
- Liopold D. Novelli - [useernamee](https://www.drupal.org/u/useernamee)

## Credits

[Jobiqo](https://www.jobiqo.com) - Module development sponsor
[Drunomics](https://www.drunomics.com) - Module development sponsor